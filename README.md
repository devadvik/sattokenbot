# SAT Token Bot #

Source code for the Telegram bot, SATTokenBot. The bot helps in managing the group as well as provide project-related infos.
It can be found as [@SATTokenBot](https://telegram.dog/SATTokenBot) on Telegram.


### Contribution guidelines ###

* Just submit a pull request with necessary details

### How do I contact you? ###

* [Telegram](https://telegram.dog/advik.dev)
* [Email](https://u.advik.dev/contact)