import telebot, os
from telebot.types import InlineKeyboardMarkup as ikm
from telebot.types import InlineKeyboardButton as ikb
from time import sleep
from alive import keep_alive

Accesstoken = os.environ['apikey']

keep_alive() #To keep the bot alive on server, not necessary tho

bot = telebot.TeleBot(Accesstoken, parse_mode="HTML")

print("Bot started successfully! Processing messages...")

#---------------------------------------#
startmsg = ", I'm SmokeandTravel (SAT) Token Group bot. I can send you info related to SAT Token."

wlcmmsg = """Welcome to <ins>Smoke And Travel</ins> Telegram group!
SAT is an online board game based project which will let users play games/tournaments and win more SAT Tokens. Players can either play for free or use their SAT tokens to win against other players. Their will be more fun things like NFTs, web apps, mobile apps, etc.
You can find more info on our <a href="https://smokeandtravel.com?ref=53">Official Website</a>.

<i>Contract Address will be released soon.</i>"""

gameownerNFT = """<b><u>Game Owner NFT info:</u></b>
If you own 1 Game owner NFT, you own <b>1/100</b> part of Smoke and Travel online game revenue.
Every 3 months you will get 1% of generated profit/revenue made by table fees and NFT sales from SmokeandTravel Online game and website.
<i><u>Note:</u> It does NOT give a revenue share of merchandise sales and real life
board game sales sold on the website or by Trust Processing LLC</i>

<b><u>How to get Game Owner NFT?</u></b>
The first buyer to buy min <code>100k</code> SAT tokens will get <code>1</code> Game Owner NFT. Afterwards, the NFT price will increase at the rate of <b>10k/sale</b>.
This means the next Game Owner NFT will be worth <code>110k</code>, then <code>120k</code> and so on. The last Game Owner NFT will be worth <code>290k</code> SAT Tokens and only <b>20 Game Owner NFTs</b> will be in circulation (means <b>20%</b> revenue share of SAT will be distributed among 20 NFT owners!)"""


@bot.message_handler(commands=["start"])
def startmessag(msg):
    mrkp = ikm()
    mrkp.add(ikb("Website", url="https://smokeandtravel.com"), ikb("TG Group", url="t.me/SmokeandTravelGame"))
    bot.send_message(msg.chat.id, f"Heyo {msg.from_user.first_name}" + startmsg, reply_markup=mrkp, disable_web_page_preview=True)
        
        
@bot.message_handler(content_types=['new_chat_members'])
def DelNWel(msg):
    message_id = msg.message_id
    with open("msgids.txt", "r") as f:
        msgid = int(f.read())
    try:
        bot.delete_message(msg.chat.id, msgid)
    except Exception as e:
        print(e)
    mrkp = ikm()
    mrkp.add(ikb("Twitter", url="https://twitter.com/_smokeandtravel"), ikb("Reddit", url="https://reddit.com/r/SmokeandTravel"))
    mrkp.add(ikb("Facebook", url="https://www.facebook.com/smokeandtravel"))
    message = bot.send_message(msg.chat.id, f"Hey {msg.from_user.first_name}, {wlcmmsg}", reply_markup=mrkp, disable_web_page_preview=True)
    try:
        bot.delete_message(msg.chat.id, message_id)
    except Exception as e:
        print(e)

    with open("msgids.txt", 'w') as f:
            f.write(str(message.id))
        
        
@bot.message_handler(content_types=["left_chat_member"])
def deleteleft(msg):
    if msg.left_chat_member.id != bot.get_me().id:
        bot.delete_message(msg.chat.id, msg.message_id)


@bot.message_handler(commands=["dev"])
def whosdad(msg):
    mrkp = ikm()
    mrkp.add(ikb("Telegram", url="t.me/advik_143"), ikb("Email", url="https://u.advik.dev/email"))
    bot.reply_to(msg, "Heya, this bot was made by @advik_143. Contact my dev regarding any queries:", reply_markup=mrkp, disable_web_page_preview=True)

@bot.message_handler(commands=['ownernft'])
def ownernft(msg):
    mrkp = ikm()
    mrkp.add(ikb("Buy SAT", url="https://smokeandtravel.com/?ref=53"))
    bot.reply_to(msg, gameownerNFT, reply_markup=mrkp)

@bot.message_handler(content_types=["text"])
def normaltext(msg):
    keyword = msg.text.lower()

    sale = ["presale", "sale", "buy", "pre-sale", "presale?", "privatesale"]
    contract = ["contract", "contrat", "contrct", "token address"]
    swap = ["swap", "swapping", "swaping", "coin to token"]
    airdrop = ["airdrop", "airdop", "free token", "free sat"]
    project = ["good project", "nice project", "great project", "strong project", "perfect project", "gud project"]

    truesale = list(filter(lambda x: x in keyword, sale))
    truecontract = list(filter(lambda x: x in keyword, contract))
    trueswap = list(filter(lambda x: x in keyword, swap))
    trueairdrop = list(filter(lambda x: x in keyword, airdrop))
    trueproject = list(filter(lambda x: x in keyword, project))

    if truesale != []:
        bot.reply_to(msg, "Presale is live on our website! You can buy SAT on our <a href='https://smokeandtravel.com?ref=53'> Official Website</a>", disable_web_page_preview=True)
    if truecontract != []:
        bot.reply_to(msg, "Contract Address will be released soon:)")
    if trueswap != []:
        bot.reply_to(msg, f"Heya {msg.from_user.first_name}, for old buyers, swapping process from coins to tokens will be released soon.\n<b><ins>Disclaimer: </ins></b>You're eligible for swap only if you bought SDOGE token during presale or on pancakeswap. Airdrop users are NOT Valid!")
    if trueairdrop != []:
        bot.reply_to(msg, f"Heyo {msg.from_user.first_name}, you can participate in our ongoing SAT Token Airdrop from our airdrop bot: @SmokeAndTravelBot\n\nLast Date: 27th January\nDistribution Date: 4th February", disable_web_page_preview=True)
    if trueproject != []:
        bot.reply_to(msg, "Thanks alot for your support!")
       

while True:
    try:
        bot.polling(0.04)
        telebot.apihelper.SESSION_TIME_TO_LIVE = 2500
    except:
        sleep(0.05)

